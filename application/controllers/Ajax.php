<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Ajax extends CI_Controller
{
    public function edit_kelas()
    {
        if ($this->input->is_ajax_request()) {
            $kelas = decrypt_url($this->input->post('id_kelas'));
            $data_kelas = $this->db->get_where('kelas', ['id_kelas' => $kelas])->row();
            echo json_encode($data_kelas);
        } else {
            redirect('eror');
        }
    }

    public function edit_mapel()
    {
        if ($this->input->is_ajax_request()) {
            $mapel = decrypt_url($this->input->post('id_mapel'));
            $data_mapel = $this->db->get_where('mapel', ['id_mapel' => $mapel])->row();
            echo json_encode($data_mapel);
        } else {
            redirect('eror');
        }
    }

    public function edit_siswa()
    {
        if ($this->input->is_ajax_request()) {
            $siswa = decrypt_url($this->input->post('id_siswa'));
            $data_siswa = $this->db->get_where('siswa', ['id_siswa' => $siswa])->row();
            echo json_encode($data_siswa);
        } else {
            redirect('eror');
        }
    }

    public function edit_guru()
    {
        if ($this->input->is_ajax_request()) {
            $guru = decrypt_url($this->input->post('id_guru'));
            $data_guru = $this->db->get_where('guru', ['id_guru' => $guru])->row();
            echo json_encode($data_guru);
        } else {
            redirect('eror');
        }
    }
    public function edit_ortu()
    {
        if ($this->input->is_ajax_request()) {
            $ortu = decrypt_url($this->input->post('id_ortu'));
            $data_ortu = $this->db->get_where('ortu', ['id_ortu' => $ortu])->row();
            echo json_encode($data_ortu);
        } else {
            redirect('eror');
        }
    }

    public function guru_kelas()
    {
        if ($this->input->is_ajax_request()) {
            $id_guru = decrypt_url($this->input->post('id_guru'));
            $id_kelas = $this->input->post('id_kelas');

            $kelass = $this->db->get_where('kelas', ['id_kelas' => $id_kelas])->row_array();
            $kelas = $kelass['nama_kelas'];


            $data = [
                'guru' => $id_guru,
                'kelas' => $id_kelas,
                'nama_kelas' => $kelas
            ];

            $result = $this->db->get_where('guru_kelas', $data);

            if ($result->num_rows() < 1) {
                $this->db->insert('guru_kelas', $data);
            } else {
                $this->db->delete('guru_kelas', $data);
            }

            $this->session->set_flashdata('pesan', "
                swal({
                    title: 'Berhasil!',
                    text: 'data diubah',
                    type: 'success',
                    padding: '2em'
                    })
                ");
        } else {
            redirect('eror');
        }
    }

    public function guru_mapel()
    {
        if ($this->input->is_ajax_request()) {
            $id_guru = decrypt_url($this->input->post('id_guru'));
            $id_mapel = $this->input->post('id_mapel');

            $mapels = $this->db->get_where('mapel', ['id_mapel' => $id_mapel])->row_array();
            $mapel = $mapels['nama_mapel'];


            $data = [
                'guru' => $id_guru,
                'mapel' => $id_mapel,
                'nama_mapel' => $mapel
            ];

            $result = $this->db->get_where('guru_mapel', $data);

            if ($result->num_rows() < 1) {
                $this->db->insert('guru_mapel', $data);
            } else {
                $this->db->delete('guru_mapel', $data);
            }

            $this->session->set_flashdata('pesan', "
                swal({
                    title: 'Berhasil!',
                    text: 'data diubah',
                    type: 'success',
                    padding: '2em'
                    })
                ");
        } else {
            redirect('eror');
        }
    }

    public function chat_materi()
    {
        if ($this->input->is_ajax_request()) {
            $kode_materi = $this->input->post('kode_materi');
            $chat_materi = $this->input->post('chat_materi');
            $user = $this->db->get_where('guru', ['id_guru' => $this->session->userdata('id')])->row();
            if (!$user) {
                $user = $this->db->get_where('siswa', ['id_siswa' => $this->session->userdata('id')])->row();
            }
            $data = [
                'materi' => $kode_materi,
                'nama' => $this->session->userdata('nama'),
                'gambar' => $user->avatar,
                'email' => $this->session->userdata('email'),
                'text' => $chat_materi,
                'date_created' => time()
            ];

            $this->db->insert('chat_materi', $data);
        } else {
            redirect('eror');
        }
    }

    public function get_chat_materi()
    {
        if ($this->input->is_ajax_request()) {
            $kode_materi = $this->input->post('kode_materi');
            $chat_materi = $this->db->get_where('chat_materi', ['materi' => $kode_materi])->result();

            foreach ($chat_materi as $chat) {
                echo '
                    <div class="media">
                    <div class="avatar avatar-sm avatar-indicators avatar-online">
                        <img alt="avatar" src="' . base_url('assets/app-assets/user/') . $chat->gambar . '" class="rounded-circle" />
                    </div>
                    <div class="media-body">
                        <h5 class="media-heading"><span class="media-title"> ' . $chat->nama . '</h5>
                        <p class="media-text" style="white-space: pre-line; margin-top: -20px;">
                            ' . $chat->text . '
                        </p>
                        <hr>
                    </div>
                </div>
            ';
            }
            exit;
        } else {
            redirect('eror');
        }
    }

    public function chat_tugas()
    {
        if ($this->input->is_ajax_request()) {
            $kode_tugas = $this->input->post('kode_tugas');
            $chat_tugas = $this->input->post('chat_tugas');
            $user = $this->db->get_where('guru', ['id_guru' => $this->session->userdata('id')])->row();
            if (!$user) {
                $user = $this->db->get_where('siswa', ['id_siswa' => $this->session->userdata('id')])->row();
            }
            $data = [
                'tugas' => $kode_tugas,
                'nama' => $this->session->userdata('nama'),
                'email' => $this->session->userdata('email'),
                'gambar' => $user->avatar,
                'text' => $chat_tugas,
                'date_created' => time()
            ];

            $this->db->insert('chat_tugas', $data);
        } else {
            redirect('eror');
        }
    }

    public function get_chat_tugas()
    {
        if ($this->input->is_ajax_request()) {
            $kode_tugas = $this->input->post('kode_tugas');
            $chat_tugas = $this->db->get_where('chat_tugas', ['tugas' => $kode_tugas])->result();

            foreach ($chat_tugas as $chat) {
                echo '
                    <div class="media">
                    <div class="avatar avatar-sm avatar-indicators avatar-online">
                        <img alt="avatar" src="' . base_url('assets/app-assets/user/') . $chat->gambar . '" class="rounded-circle" />
                    </div>
                    <div class="media-body">
                        <h5 class="media-heading">' . $chat->nama . '</h5>
                        <p class="media-text" style="white-space: pre-line; margin-top: -20px;">
                            ' . $chat->text . '
                        </p>
                        <hr>
                    </div>
                </div>
            ';
            }
            exit;
        } else {
            redirect('eror');
        }
    }

    public function edit_materi()
    {
        if ($this->input->is_ajax_request()) {
            $materi = decrypt_url($this->input->post('id_materi'));
            $data_materi = $this->db->get_where('materi', ['id_materi' => $materi])->row();
            echo json_encode($data_materi);
        } else {
            redirect('eror');
        }
    }

    public function edit_tugas()
    {
        if ($this->input->is_ajax_request()) {
            $tugas = decrypt_url($this->input->post('id_tugas'));
            $data_tugas = $this->db->get_where('tugas', ['id_tugas' => $tugas])->row();
            echo json_encode($data_tugas);
        } else {
            redirect('eror');
        }
    }

    public function time_now()
    {
        date_default_timezone_set('Asia/Jakarta');
        echo date('Y-m-d H:i:s', time());
    }

    public function cek_ujian()
    {
        if ($this->input->is_ajax_request()) {
            $kode_ujian = $this->input->post('kode_ujian');
            $waktu = $this->input->post('waktu');
            $ujian = $this->db->get_where('ujian', ['kode_ujian' => $kode_ujian])->row();

            if (strtotime($waktu) > strtotime($ujian->waktu_berakhir)) {
                echo "1";
            } else {
                echo "0";
            }
            exit;
        } else {
            redirect('eror');
        }
    }

    // public function kerjakan_tugas()
    // {
    //     if ($this->input->is_ajax_request()) {
    //         $siswa = decrypt_url($this->input->post('siswa'));
    //         $id_tugas = decrypt_url($this->input->post('tugas'));
    //         $tugas = $this->db->get_where('tugas', ['id_tugas' => $id_tugas])->row();
    //         $tugas_siswa = $this->db->get_where('tugas_siswa', ['tugas' => $tugas->kode_tugas, 'siswa' => $siswa])->row();
    //         echo json_encode($tugas_siswa);
    //     } else {
    //         redirect('eror');
    //     }
    // }
}
