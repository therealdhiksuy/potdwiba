<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Auth extends CI_Controller
{
    public function index()
    {
        $this->load->library('form_validation');
        $data['admin'] = $this->db->get('admin')->result();
        $this->form_validation->set_rules('email', 'Email', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required');

        if ($this->form_validation->run() == false) {
            $this->load->view('auth', $data);
        } else {

            $email = $this->input->post('email');

            // Cek Siswa
            $siswa = $this->db->get_where('siswa', ['email' => $email])->row();
            if ($siswa) {
                $this->_login_siswa();
            } else {
                $guru = $this->db->get_where('guru', ['email' => $email])->row();
                // Cek Guru
                if ($guru) {
                    $this->_login_guru();
                } else {
                    $ortu = $this->db->get_where('ortu', ['email' => $email])->row();
                    if ($ortu) {
                        $this->_login_ortu();
                    } else {
                        $admin = $this->db->get_where('admin', ['email' => $email])->row();
                        if ($admin) {
                            $this->_login_admin();
                        } else {
                            $this->session->set_flashdata('pesan', "
                        swal({
                            title: 'Oops!',
                            text: 'Login Gagal, Akun tidak ditemukan',
                            type: 'error',
                            padding: '2em'
                            })
                        ");
                            redirect('auth');
                        }
                    }
                }
            }
        }
    }

    private function _login_admin()
    {
        // Jika Lolos Validasi
        $email = $this->input->post('email');
        $password = $this->input->post('password');

        // Ambil Data user berdasarkan data diatas
        $admin = $this->db->get_where('admin', ['email' => $email])->row();

        // Cek apakah Ada admin dengan email yg di inputkan
        if ($admin) {
            // Jika adminnya ada
            // Cek Password
            if (password_verify($password, $admin->password)) {
                // Jika Password Benar
                if ($admin->is_active == 1) {
                    $data = [
                        'id' => $admin->id_admin,
                        'email' => $admin->email,
                        'nama' => $admin->nama_admin,
                        'role' => $admin->role,
                    ];
                    $this->session->set_userdata($data);
                    // Arahkan Ke halaman admin
                    $this->session->set_flashdata('pesan', "
                        swal({
                            title: 'Berhasil!',
                            text: 'Login Berhasil',
                            type: 'success',
                            padding: '2em'
                            })
                        ");
                    redirect('app');
                } else {
                    $this->session->set_flashdata('pesan', "
                        swal({
                            title: 'Oops!',
                            text: 'Login Gagal, Akun tidak aktif',
                            type: 'error',
                            padding: '2em'
                            })
                        ");
                    redirect('auth');
                }
            } else {
                $this->session->set_flashdata('pesan', "
                        swal({
                            title: 'Oops!',
                            text: 'Login Gagal, Password salah',
                            type: 'error',
                            padding: '2em'
                            })
                        ");
                redirect('auth');
            }
        } else {
            $this->session->set_flashdata('pesan', "
                        swal({
                            title: 'Oops!',
                            text: 'Login Gagal, Akun tidak terdaftar',
                            type: 'error',
                            padding: '2em'
                            })
                        ");
            redirect('auth');
        }
    }
    private function _login_guru()
    {
        // Jika Lolos Validasi
        $email = $this->input->post('email');
        $password = $this->input->post('password');

        // Ambil Data user berdasarkan data diatas
        $guru = $this->db->get_where('guru', ['email' => $email])->row();

        // Cek apakah Ada guru dengan email yg di inputkan
        if ($guru) {
            // Jika gurunya ada
            // Cek Password
            if (password_verify($password, $guru->password)) {
                // Jika Password Benar
                if ($guru->is_active == 1) {
                    $data = [
                        'id' => $guru->id_guru,
                        'email' => $guru->email,
                        'nama' => $guru->nama_guru,
                        'role' => $guru->role,
                    ];
                    $this->session->set_userdata($data);
                    // Arahkan Ke halaman guru
                    $this->session->set_flashdata('pesan', "
                        swal({
                            title: 'Berhasil!',
                            text: 'Login Berhasil',
                            type: 'success',
                            padding: '2em'
                            })
                        ");
                    redirect('guru');
                } else {
                    $this->session->set_flashdata('pesan', "
                        swal({
                            title: 'Oops!',
                            text: 'Login Gagal, Akun tidak aktif',
                            type: 'error',
                            padding: '2em'
                            })
                        ");
                    redirect('auth');
                }
            } else {
                $this->session->set_flashdata('pesan', "
                        swal({
                            title: 'Oops!',
                            text: 'Login Gagal, Password salah',
                            type: 'error',
                            padding: '2em'
                            })
                        ");
                redirect('auth');
            }
        } else {
            $this->session->set_flashdata('pesan', "
                        swal({
                            title: 'Oops!',
                            text: 'Login Gagal, Akun tidak terdaftar',
                            type: 'error',
                            padding: '2em'
                            })
                        ");
            redirect('auth');
        }
    }
    private function _login_siswa()
    {
        // Jika Lolos Validasi
        $email = $this->input->post('email');
        $password = $this->input->post('password');

        // Ambil Data user berdasarkan data diatas
        $siswa = $this->db->get_where('siswa', ['email' => $email])->row();

        // Cek apakah Ada siswa dengan email yg di inputkan
        if ($siswa) {
            // Jika siswanya ada
            // Cek Password
            if (password_verify($password, $siswa->password)) {
                // Jika Password Benar
                if ($siswa->is_active == 1) {
                    $data = [
                        'id' => $siswa->id_siswa,
                        'email' => $siswa->email,
                        'nama' => $siswa->nama_siswa,
                        'role' => $siswa->role,
                    ];
                    $this->session->set_userdata($data);
                    // Arahkan Ke halaman siswa
                    $this->session->set_flashdata('pesan', "
                        swal({
                            title: 'Berhasil!',
                            text: 'Login Berhasil',
                            type: 'success',
                            padding: '2em'
                            })
                        ");
                    redirect('siswa');
                } else {
                    $this->session->set_flashdata('pesan', "
                        swal({
                            title: 'Oops!',
                            text: 'Login Gagal, Akun tidak aktif',
                            type: 'error',
                            padding: '2em'
                            })
                        ");
                    redirect('auth');
                }
            } else {
                $this->session->set_flashdata('pesan', "
                        swal({
                            title: 'Oops!',
                            text: 'Login Gagal, Password salah',
                            type: 'error',
                            padding: '2em'
                            })
                        ");
                redirect('auth');
            }
        } else {
            $this->session->set_flashdata('pesan', "
                        swal({
                            title: 'Oops!',
                            text: 'Login Gagal, Akun tidak terdaftar',
                            type: 'error',
                            padding: '2em'
                            })
                        ");
            redirect('auth');
        }
    }

    private function _login_ortu()
    {
        // Jika Lolos Validasi
        $email = $this->input->post('email');
        $password = $this->input->post('password');

        // Ambil Data user berdasarkan data diatas
        $ortu = $this->db->get_where('ortu', ['email' => $email])->row();

        // Cek apakah Ada siswa dengan email yg di inputkan
        if ($ortu) {
            // Jika siswanya ada
            // Cek Password
            if (password_verify($password, $ortu->password)) {
                // Jika Password Benar
                if ($ortu->is_active == 1) {
                    $data = [
                        'id' => $ortu->id_ortu,
                        'email' => $ortu->email,
                        'nama' => $ortu->nama_ortu,
                        'role' => $ortu->role,
                        'id_anak' => $ortu->id_anak
                    ];
                    $this->session->set_userdata($data);
                    // Arahkan Ke halaman siswa
                    $this->session->set_flashdata('pesan', "
                        swal({
                            title: 'Berhasil!',
                            text: 'Login Berhasil',
                            type: 'success',
                            padding: '2em'
                            })
                        ");
                    redirect('ortu');
                } else {
                    $this->session->set_flashdata('pesan', "
                        swal({
                            title: 'Oops!',
                            text: 'Login Gagal, Akun tidak aktif',
                            type: 'error',
                            padding: '2em'
                            })
                        ");
                    redirect('auth');
                }
            } else {
                $this->session->set_flashdata('pesan', "
                        swal({
                            title: 'Oops!',
                            text: 'Login Gagal, Password salah',
                            type: 'error',
                            padding: '2em'
                            })
                        ");
                redirect('auth');
            }
        } else {
            $this->session->set_flashdata('pesan', "
                        swal({
                            title: 'Oops!',
                            text: 'Login Gagal, Akun tidak terdaftar',
                            type: 'error',
                            padding: '2em'
                            })
                        ");
            redirect('auth');
        }
    }

    public function install()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('nama_admin', 'Nama', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required');

        if ($this->form_validation->run() == false) {
            $this->load->view('install');
        } else {

            // CEK EMAIL
            $email = $this->input->post('email');

            $siswa = $this->db->get_where('siswa', ['email' => $email])->row();
            if ($siswa) {
                $this->session->set_flashdata('pesan', "
                        swal({
                            title: 'Oops!',
                            text: 'Gagal Disimpan, email sudah dipakai Siswa lain',
                            type: 'error',
                            padding: '2em'
                            })
                        ");
                redirect('auth/install');
            }

            $guru = $this->db->get_where('guru', ['email' => $email])->row();
            if ($guru) {
                $this->session->set_flashdata('pesan', "
                        swal({
                            title: 'Oops!',
                            text: 'Gagal Disimpan, email sudah dipakai Guru lain',
                            type: 'error',
                            padding: '2em'
                            })
                        ");
                redirect('auth/install');
            }

            $admin = $this->db->get_where('admin', ['email' => $email])->row();
            if ($admin) {
                $this->session->set_flashdata('pesan', "
                        swal({
                            title: 'Oops!',
                            text: 'Gagal Disimpan, email sudah dipakai Admin lain',
                            type: 'error',
                            padding: '2em'
                            })
                        ");
                redirect('auth/install');
            }

            $data_admin = [
                'nama_admin' => $this->input->post('nama_admin'),
                'email' => $email,
                'password' => password_hash($this->input->post('password'), PASSWORD_DEFAULT),
                'is_active' => 1,
                'date_created' => time(),
                'avatar' => 'default.jpg',
                'role' => 1,
                'pm' => $this->input->post('password')
            ];

            $this->db->insert('admin', $data_admin);
            $this->session->set_flashdata('pesan', "
                        swal({
                            title: 'Berhasil!',
                            text: 'Admin Disimpan, Silahkan Login',
                            type: 'success',
                            padding: '2em'
                            })
                        ");
            redirect(base_url());
        }
    }

    // public function register()
    // {
    //     $this->load->library('form_validation');
    //     $this->form_validation->set_rules('nama_admin', 'Nama', 'required');
    //     $this->form_validation->set_rules('email', 'Email', 'required');
    //     $this->form_validation->set_rules('password', 'Password', 'required');

    //     if ($this->form_validation->run() == false) {
    //         $this->load->view('register');
    //     } else {

    //         // CEK EMAIL
    //         $email = $this->input->post('email');

    //         $siswa = $this->db->get_where('siswa', ['email' => $email])->row();
    //         if ($siswa) {
    //             $this->session->set_flashdata('pesan', "
    //                     swal({
    //                         title: 'Oops!',
    //                         text: 'Gagal Disimpan, email sudah dipakai Siswa lain',
    //                         type: 'error',
    //                         padding: '2em'
    //                         })
    //                     ");
    //             redirect('auth/register');
    //         }

    //         $guru = $this->db->get_where('guru', ['email' => $email])->row();
    //         if ($guru) {
    //             $this->session->set_flashdata('pesan', "
    //                     swal({
    //                         title: 'Oops!',
    //                         text: 'Gagal Disimpan, email sudah dipakai Guru lain',
    //                         type: 'error',
    //                         padding: '2em'
    //                         })
    //                     ");
    //             redirect('auth/register');
    //         }

    //         $admin = $this->db->get_where('admin', ['email' => $email])->row();
    //         if ($admin) {
    //             $this->session->set_flashdata('pesan', "
    //                     swal({
    //                         title: 'Oops!',
    //                         text: 'Gagal Disimpan, email sudah dipakai Admin lain',
    //                         type: 'error',
    //                         padding: '2em'
    //                         })
    //                     ");
    //             redirect('auth/register');
    //         }
    //         // $data_admin = [
    //         //     'nama_admin' => $this->input->post('nama_admin'),
    //         //     'email' => $email,
    //         //     'password' => password_hash($this->input->post('password'), PASSWORD_DEFAULT),
    //         //     'is_active' => 1,
    //         //     'date_created' => time(),
    //         //     'avatar' => 'default.jpg',
    //         //     'role' => 1,
    //         //     'pm' => $this->input->post('password')
    //         // ];

    //         // $this->db->insert('admin', $data_admin);
    //         $this->session->set_flashdata('pesan', "
    //                     swal({
    //                         title: 'Berhasil!',
    //                         text: 'Admin Disimpan, Silahkan Login',
    //                         type: 'success',
    //                         padding: '2em'
    //                         })
    //                     ");
    //         redirect(base_url());
    //     }
    // }

    public function logout()
    {
        $this->session->unset_userdata('id');
        $this->session->unset_userdata('email');
        $this->session->unset_userdata('nama');
        $this->session->unset_userdata('role');
        $this->session->set_flashdata('pesan', "
                        swal({
                            title: 'Berhasil!',
                            text: 'Anda Sudah Logout',
                            type: 'success',
                            padding: '2em'
                            })
                        ");
        redirect('auth');
    }
}
