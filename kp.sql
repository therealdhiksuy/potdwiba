-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 15, 2022 at 02:08 PM
-- Server version: 10.1.40-MariaDB
-- PHP Version: 7.3.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kp`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id_admin` int(11) NOT NULL,
  `nama_admin` varchar(255) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  `is_active` int(11) NOT NULL,
  `date_created` int(11) NOT NULL,
  `avatar` varchar(255) NOT NULL,
  `role` int(11) NOT NULL,
  `pm` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id_admin`, `nama_admin`, `email`, `password`, `is_active`, `date_created`, `avatar`, `role`, `pm`) VALUES
(13, 'Admin', 'therealdhiksuy@gmail.com', '$2y$10$uvbnQzkhfyXYV3S5qJ9Xu.7WSizP00tTKNt14XpRHHvql/gqj3tNK', 1, 1645171959, 'default.jpg', 1, 'Priandhika404!');

-- --------------------------------------------------------

--
-- Table structure for table `chat_materi`
--

CREATE TABLE `chat_materi` (
  `id_chat_materi` int(11) NOT NULL,
  `materi` varchar(100) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `gambar` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `text` longtext NOT NULL,
  `date_created` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `chat_materi`
--

INSERT INTO `chat_materi` (`id_chat_materi`, `materi`, `nama`, `gambar`, `email`, `text`, `date_created`) VALUES
(71, 'R7CJveUg', 'Madani', 'default.jpg', 'therealdhiksuy2@gmail.com', 'Bu saya belum paham', 1645172517),
(72, 'R7CJveUg', 'Pak Asep', 'default.jpg', 'asep123@gmail.com', 'TOlong dibaca lagi', 1645172541),
(73, 'R7CJveUg', 'Dhika', 'default.jpg', 'therealdhiksuy3@gmail.com', 'Assalamualaikum', 1645172839);

-- --------------------------------------------------------

--
-- Table structure for table `chat_tugas`
--

CREATE TABLE `chat_tugas` (
  `id_chat_tugas` int(11) NOT NULL,
  `tugas` varchar(100) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `email` varchar(255) NOT NULL,
  `gambar` varchar(255) NOT NULL,
  `text` longtext NOT NULL,
  `date_created` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `file`
--

CREATE TABLE `file` (
  `id_file` int(11) NOT NULL,
  `kode_file` varchar(100) NOT NULL,
  `nama_file` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `file`
--

INSERT INTO `file` (`id_file`, `kode_file`, `nama_file`) VALUES
(1, 'XJ6SPvnZ', 'aaaa.jpeg'),
(2, 'vdrkzswb', '1.jpg'),
(3, '6JjNHayl', '202112_86932344.pdf'),
(11, 'etbs39Y0', '1.xlsx'),
(13, 'h76YKv12', 'rekap_bulan1.htm'),
(18, 'Ns2z6Zyx', 'rekap_bulan3.htm'),
(19, 'Ns2z6Zyx', 'rekap_bulan4.htm'),
(20, 'YidV85bW', 'rekap_bulan5.htm'),
(21, 'GNSFBXYH', 'rekap_bulan.htm'),
(22, 'Hjyu6pMA', 'rekap_bulan2.htm'),
(25, 'NuzTfROn', 'Laporan_Kerja_Praktek_Revisi_II.docx'),
(26, 'NFpcEH1f', 'Laporan_Kerja_Praktek_Madani_Geusanrumaksa_dan_Priandika_(1)_(2).docx'),
(27, '5uOsiMHq', 'Laporan_Kerja_Praktek_Revisi_II1.docx'),
(28, 'gSoDUcpM', 'template_(1).xlsx'),
(29, 'azo9x0t1', 'realusecase_drawio_(8).png'),
(30, 'FUijMfHV', 'realusecase_drawio_(8)1.png'),
(31, 'HORoBVsa', 'realusecase_drawio_(8)2.png'),
(32, 'XO7gxomy', 'realusecase_drawio_(8)3.png'),
(34, 'QOtizckn', 'realusecase_drawio_(9).png'),
(35, 'QOtizckn', 'a.mp4'),
(36, 'AXKeCUdi', 'a.png'),
(37, '3vL5dVOb', 'LKP_MadaniGeusanrumaksa_PriandhikaLubis.docx'),
(38, 'oxcT1qzA', 'rekap.docx'),
(39, '73TfSRom', 'a1.png'),
(40, '73TfSRom', 'a1.mp4'),
(41, 'fjUTrwY7', 'a2.png'),
(42, 'fjUTrwY7', 'a2.mp4'),
(43, 'R7CJveUg', 'a3.png'),
(44, 'R7CJveUg', 'a3.mp4'),
(45, 'lo1erDdi', 'a4.png'),
(46, 'hjqS1nwx', 'a41.png');

-- --------------------------------------------------------

--
-- Table structure for table `guru`
--

CREATE TABLE `guru` (
  `id_guru` int(11) NOT NULL,
  `nama_guru` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role` int(1) NOT NULL,
  `is_active` int(1) NOT NULL,
  `date_created` int(11) NOT NULL,
  `avatar` varchar(255) NOT NULL,
  `guru_kelas` varchar(255) DEFAULT NULL,
  `guru_mapel` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `guru`
--

INSERT INTO `guru` (`id_guru`, `nama_guru`, `email`, `password`, `role`, `is_active`, `date_created`, `avatar`, `guru_kelas`, `guru_mapel`) VALUES
(2, 'Pak Asep', 'asep123@gmail.com', '$2y$10$oE6EGvoOpWCRIx26GiZryudO.aINKwSFw1rqf6I6uG4Ln6g1Z9I9e', 3, 1, 1645172149, 'default.jpg', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `guru_kelas`
--

CREATE TABLE `guru_kelas` (
  `id_guru_kelas` int(11) NOT NULL,
  `guru` int(11) NOT NULL,
  `kelas` int(11) NOT NULL,
  `nama_kelas` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `guru_kelas`
--

INSERT INTO `guru_kelas` (`id_guru_kelas`, `guru`, `kelas`, `nama_kelas`) VALUES
(11, 12, 1, 'X-IPA-1'),
(12, 16, 5, 'VII-A'),
(13, 16, 6, 'VII-B'),
(14, 17, 5, 'VII-A'),
(16, 19, 10, 'VII-F'),
(18, 18, 10, 'VII-F'),
(19, 18, 7, 'VII-C'),
(20, 20, 5, 'VII-A'),
(22, 1, 7, 'VII-C'),
(23, 2, 5, 'VII-A'),
(24, 2, 6, 'VII-B');

-- --------------------------------------------------------

--
-- Table structure for table `guru_mapel`
--

CREATE TABLE `guru_mapel` (
  `id_guru_mapel` int(11) NOT NULL,
  `guru` int(11) NOT NULL,
  `mapel` int(11) NOT NULL,
  `nama_mapel` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `guru_mapel`
--

INSERT INTO `guru_mapel` (`id_guru_mapel`, `guru`, `mapel`, `nama_mapel`) VALUES
(8, 12, 1, 'Kimia lanjutan'),
(9, 16, 2, 'Fisika dasar'),
(10, 16, 1, 'Kimia lanjutan'),
(11, 17, 1, 'Kimia lanjutan'),
(12, 17, 2, 'Fisika dasar'),
(13, 18, 2, 'Fisika dasar'),
(14, 19, 4, 'Bahasa Inggris'),
(15, 18, 1, 'Kimia lanjutan'),
(16, 20, 1, 'Kimia'),
(17, 1, 5, 'Bahasa Indonesia'),
(18, 2, 6, 'Matematika');

-- --------------------------------------------------------

--
-- Table structure for table `kelas`
--

CREATE TABLE `kelas` (
  `id_kelas` int(11) NOT NULL,
  `nama_kelas` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kelas`
--

INSERT INTO `kelas` (`id_kelas`, `nama_kelas`) VALUES
(5, 'VII-A'),
(6, 'VII-B'),
(7, 'VII-C'),
(8, 'VII-D'),
(9, 'VII-E'),
(10, 'VII-F'),
(11, 'VII-G'),
(12, 'VII-H'),
(13, 'VII-I'),
(14, 'VIII-A'),
(15, 'VIII-B'),
(16, 'VIII-C'),
(17, 'VIII-D'),
(18, 'VIII-E'),
(19, 'VIII-F'),
(20, 'VIII-G'),
(21, 'VIII-H'),
(22, 'VIII-I'),
(23, 'IX-A'),
(24, 'IX-B'),
(25, 'IX-C'),
(26, 'IX-D'),
(27, 'IX-E'),
(28, 'IX-F'),
(29, 'IX-G'),
(30, 'IX-H'),
(31, 'IX-I');

-- --------------------------------------------------------

--
-- Table structure for table `mapel`
--

CREATE TABLE `mapel` (
  `id_mapel` int(11) NOT NULL,
  `nama_mapel` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mapel`
--

INSERT INTO `mapel` (`id_mapel`, `nama_mapel`) VALUES
(1, 'Kimia'),
(2, 'Fisika dasar'),
(4, 'Bahasa Inggris'),
(5, 'Bahasa Indonesia'),
(6, 'Matematika');

-- --------------------------------------------------------

--
-- Table structure for table `materi`
--

CREATE TABLE `materi` (
  `id_materi` int(11) NOT NULL,
  `kode_materi` varchar(100) NOT NULL,
  `nama_materi` varchar(255) NOT NULL,
  `guru` int(11) NOT NULL,
  `mapel` int(11) NOT NULL,
  `kelas` int(11) NOT NULL,
  `text_materi` longtext NOT NULL,
  `date_created` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `materi`
--

INSERT INTO `materi` (`id_materi`, `kode_materi`, `nama_materi`, `guru`, `mapel`, `kelas`, `text_materi`, `date_created`) VALUES
(2, 'R7CJveUg', 'Bab 1 ', 2, 6, 5, 'Tolong pahami', 1645172435);

-- --------------------------------------------------------

--
-- Table structure for table `materi_siswa`
--

CREATE TABLE `materi_siswa` (
  `id_materi_siswa` int(11) NOT NULL,
  `materi` varchar(100) NOT NULL,
  `kelas` int(11) NOT NULL,
  `mapel` int(11) NOT NULL,
  `siswa` int(11) NOT NULL,
  `dilihat` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `materi_siswa`
--

INSERT INTO `materi_siswa` (`id_materi_siswa`, `materi`, `kelas`, `mapel`, `siswa`, `dilihat`) VALUES
(1, 'fjUTrwY7', 7, 5, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `ortu`
--

CREATE TABLE `ortu` (
  `id_ortu` int(255) NOT NULL,
  `nama_ortu` varchar(255) NOT NULL,
  `anak` varchar(255) NOT NULL,
  `id_anak` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role` int(255) NOT NULL,
  `is_active` int(255) NOT NULL,
  `avatar` varchar(255) NOT NULL,
  `date_created` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ortu`
--

INSERT INTO `ortu` (`id_ortu`, `nama_ortu`, `anak`, `id_anak`, `email`, `password`, `role`, `is_active`, `avatar`, `date_created`) VALUES
(3, 'Bapak Madani', 'Madani', '3', 'bapakmadani@gmail.com', '$2y$10$/Te3Ww5ZIo3K25NW64741efYb9Tgrnv3uCOag0mYRlsTj7dW/u/ki', 4, 1, 'default.jpg', 1645172191);

-- --------------------------------------------------------

--
-- Table structure for table `rekap`
--

CREATE TABLE `rekap` (
  `id_rekap` int(255) NOT NULL,
  `kode_rekap` varchar(255) NOT NULL,
  `nama_rekap` varchar(255) NOT NULL,
  `guru` varchar(255) NOT NULL,
  `mapel` varchar(255) NOT NULL,
  `kelas` varchar(255) NOT NULL,
  `text_rekap` varchar(255) NOT NULL,
  `date_created` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rekap`
--

INSERT INTO `rekap` (`id_rekap`, `kode_rekap`, `nama_rekap`, `guru`, `mapel`, `kelas`, `text_rekap`, `date_created`) VALUES
(1, 'hjqS1nwx', 'Absensi Bulan January', '2', '6', '5', 'Rekap Absen', '1645174038');

-- --------------------------------------------------------

--
-- Table structure for table `siswa`
--

CREATE TABLE `siswa` (
  `id_siswa` int(11) NOT NULL,
  `no_induk_siswa` varchar(100) NOT NULL,
  `nama_siswa` varchar(255) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  `jenis_kelamin` varchar(100) NOT NULL,
  `kelas` int(11) NOT NULL,
  `role` int(1) NOT NULL,
  `is_active` int(11) NOT NULL,
  `date_created` int(11) NOT NULL,
  `avatar` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `siswa`
--

INSERT INTO `siswa` (`id_siswa`, `no_induk_siswa`, `nama_siswa`, `email`, `password`, `jenis_kelamin`, `kelas`, `role`, `is_active`, `date_created`, `avatar`) VALUES
(3, '411', 'Madani', 'therealdhiksuy2@gmail.com', '$2y$10$bE5C..Uy4GkBolKsTlpp9.8VWcDllkWRT0zuUco32sH4Fsf5C.1E.', 'Laki - Laki', 5, 2, 1, 1645172057, 'default.jpg'),
(4, '412', 'Dhika', 'therealdhiksuy3@gmail.com', '$2y$10$deUGKlQxV25fpErZl3PLQOIESWUOkCJlyKpZ4KjUpSKkKlfLB/YB.', 'Laki - Laki', 5, 2, 1, 1645172639, 'default.jpg'),
(5, '412', 'Wildan', 'facelesskrt@gmail.com', '$2y$10$61buVCSKncBDzBbuZLB91uwbu/WqcAsE1FVaxe6hCzoWerz2Gdk/a', 'Laki - Laki', 5, 2, 1, 1645172659, 'default.jpg'),
(6, '413', 'Wildan Sihombing', 'facelesskrt@gmail.com', '$2y$10$8KMlHCNfDvlkkoXY7NqigecQ6grdjJrPiPSigF7OCLnAOAv9rV566', 'Laki - Laki', 5, 2, 1, 1645172687, 'default.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `tugas`
--

CREATE TABLE `tugas` (
  `id_tugas` int(11) NOT NULL,
  `kode_tugas` varchar(100) NOT NULL,
  `kelas` int(11) NOT NULL,
  `mapel` int(11) NOT NULL,
  `guru` int(11) NOT NULL,
  `nama_tugas` varchar(255) NOT NULL,
  `deskripsi` longtext NOT NULL,
  `date_created` int(11) NOT NULL,
  `due_date` varchar(255) NOT NULL,
  `date_updated` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tugas`
--

INSERT INTO `tugas` (`id_tugas`, `kode_tugas`, `kelas`, `mapel`, `guru`, `nama_tugas`, `deskripsi`, `date_created`, `due_date`, `date_updated`) VALUES
(1, 'yz3LmAV7', 5, 6, 2, 'Tugas Bab 1', 'TOlong dikerjakan', 1645173250, '2022-02-18 19:34', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tugas_siswa`
--

CREATE TABLE `tugas_siswa` (
  `id_tugas_siswa` int(11) NOT NULL,
  `tugas` varchar(100) NOT NULL,
  `siswa` int(11) NOT NULL,
  `text_siswa` longtext,
  `file_siswa` varchar(100) DEFAULT NULL,
  `date_send` int(11) DEFAULT NULL,
  `is_telat` int(11) DEFAULT NULL,
  `nilai` int(11) DEFAULT NULL,
  `catatan_guru` longtext
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tugas_siswa`
--

INSERT INTO `tugas_siswa` (`id_tugas_siswa`, `tugas`, `siswa`, `text_siswa`, `file_siswa`, `date_send`, `is_telat`, `nilai`, `catatan_guru`) VALUES
(1, 'NdFWQL6R', 18, 'Gapapa', '8f3Ta2Rl', 1642458972, 0, 100, 'Bagus sekali'),
(2, 'uzScY6AQ', 18, 'az', 'L0DduWXH', 1642459148, 0, 88, 'Aduh'),
(3, 'XJ6SPvnZ', 18, 'asdasdasdasdasd', 'vdrkzswb', 1642537489, 1, 90, ''),
(6, 'K5kNuSxn', 19, NULL, NULL, NULL, NULL, NULL, NULL),
(8, 'NFpcEH1f', 20, 'maap telat', '5uOsiMHq', 1644495946, 0, 80, 'Sudah bagus'),
(9, 'HORoBVsa', 20, NULL, NULL, NULL, NULL, NULL, NULL),
(10, 'p1WCFYhy', 20, NULL, NULL, NULL, NULL, NULL, NULL),
(11, 'AXKeCUdi', 22, 'Assalamualaikum saya sudah mengumpulkan', '3vL5dVOb', 1645130898, 0, 80, 'Sudah bagus'),
(12, '7KG5Zg3E', 22, NULL, NULL, NULL, NULL, NULL, NULL),
(13, 'yz3LmAV7', 3, 'Sudah', 'lo1erDdi', 1645173328, 0, 1, ''),
(14, 'yz3LmAV7', 4, NULL, NULL, NULL, NULL, NULL, NULL),
(15, 'yz3LmAV7', 5, NULL, NULL, NULL, NULL, NULL, NULL),
(16, 'yz3LmAV7', 6, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `ujian`
--

CREATE TABLE `ujian` (
  `id_ujian` int(11) NOT NULL,
  `kode_ujian` varchar(100) NOT NULL,
  `nama_ujian` varchar(255) NOT NULL,
  `guru` int(11) NOT NULL,
  `kelas` int(11) NOT NULL,
  `mapel` int(11) NOT NULL,
  `date_created` int(11) NOT NULL,
  `waktu_mulai` varchar(100) NOT NULL,
  `waktu_berakhir` varchar(100) NOT NULL,
  `jenis_ujian` int(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ujian`
--

INSERT INTO `ujian` (`id_ujian`, `kode_ujian`, `nama_ujian`, `guru`, `kelas`, `mapel`, `date_created`, `waktu_mulai`, `waktu_berakhir`, `jenis_ujian`) VALUES
(1, 'uB9fdp8wvi', 'Ujian Matematika Bab 1', 2, 5, 6, 1645173666, '2022-02-18 15:40', '2022-02-18 15:43', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `ujian_detail`
--

CREATE TABLE `ujian_detail` (
  `id_detail_ujian` int(11) NOT NULL,
  `kode_ujian` varchar(100) NOT NULL,
  `nama_soal` longtext NOT NULL,
  `pg_1` varchar(100) NOT NULL,
  `pg_2` varchar(100) NOT NULL,
  `pg_3` varchar(100) NOT NULL,
  `pg_4` varchar(100) NOT NULL,
  `jawaban` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ujian_detail`
--

INSERT INTO `ujian_detail` (`id_detail_ujian`, `kode_ujian`, `nama_soal`, `pg_1`, `pg_2`, `pg_3`, `pg_4`, `jawaban`) VALUES
(1, 'JwhLunEjeY', 'a', 'A. a', 'B. a', 'C. a', 'D. a', 'A'),
(2, 'JwhLunEjeY', 'a', 'A. a', 'B. a', 'C. a', 'D. a', 'B'),
(3, 'BaMTfzVsuH', 'Gerakan suatu benda dimana setiap titik pada benda tersebut mempunyai jarak yang tetap terhadap suatu sumbu tertentu, merupakan pengertian dari:', 'A. Gerak Translasi', 'B. Gerak Rotasi', 'C. Perpindahan', 'D. Kelajuan', 'nomaden'),
(4, 'BaMTfzVsuH', 'Sebuah balok bermassa 1,5 kg didorong ke atas oleh gaya konstan F = 15 N pada bidang miring seperti gambar. Anggap percepatan gravitasi (g) 10 ms-2 dan gesekan antara balok dan bidang miring nol. Usaha total yang dilakukan pada balok adalah ...', 'A. 15 J', 'B. 30 J', 'C. 35 J', 'D. 45 J', '100 J'),
(5, 'BaMTfzVsuH', 'Sebanyak 3 liter gas Argon bersuhu 270C pada tekanan 1 atm (1 atm = 105 Pa) berada di dalam tabung. Jika konstanta gas umum R = 8,314 J m-1K-1 dan banyaknya partikel dalam 1 mol gas 6,02 x 1023 partikel, maka banyak partikel gas Argon dalam tabung tersebut adalah ...', 'A. 0,83 x 1023 partikel', 'B. 0,72 x 1023 partikel', 'C. 0,42 x 1023 partikel', 'D. 0,22 x 1023 partikel', '0.1 x 1000 partikel'),
(6, 'BaMTfzVsuH', 'Sebuah partikel berputar  dengan 240 rpm. Jika jari-jari lintasannya 2 m, maka kelajuan linier partikel tersebut adalah:', 'A. 4 ? m/s', 'B. 6 ? m/s', 'C. 8 ? m/s', 'D. 16 ? m/s', '18 ? m/s'),
(7, 'BaMTfzVsuH', 'Sebuah mobil mulai bergerak dari keadaan diam dengan percepatan tetap 24 m/s2. Maka kecepatan mobil setelah bergerak selama 18 sekon adalah:', 'A. 2 m/s', 'B. 24 m/s', 'C. 36 m/s', 'D. 42 m/s', '50 m/s'),
(8, 'BaMTfzVsuH', 'Suatu kereta api sedang bergerak dengan kelajuan 17 m/s. Ketika melewati tanda akan memasuki stasiun, masinis memperlambat kelajuan kereta api sebesar 2,00 m/s2. Waktu yang diperlukan kereta api sampai kereta api benar-benar berhenti adalah …', 'A. 3,5', 'B. 8', 'C. 8,5', 'D. 14', '90'),
(9, 'BaMTfzVsuH', 'Waktu yang diperlukan sebuah mobil yang bergerak dengan percepatan 2 m/s2, untuk mengubah kecepatannya dari 10 m/s menjadi 30 m/s adalah:', 'A. 10 s', 'B. 20 s', 'C. 30 s', 'D. 40 s', '11 s'),
(10, 'BaMTfzVsuH', 'Sebuah peluru dengan massa 20 gram ditembakkan pada sudut elevasi 600 dan kecepatan 40 m.s-   1 seperti gambar. Jika gesekan dengan udara diabaikan, maka energi kinetik peluru pada titik tertinggi adalah ...  ', 'A. 0 joule', 'B. 4 joule', 'C. 8 joule', 'D. 12 joule', '2 joule'),
(11, 'BaMTfzVsuH', 'Kelompok besaran di bawah ini yang termasuk besaran vektor adalah . . . .', 'A. kelajuan, kuat arus, gaya', 'B. energi, usaha, banyak mol zat', 'C. kecepatan, momentum, kuat arus listrik', 'D. tegangan, intensitas cahaya, gaya', 'tegangan listrik'),
(12, 'BaMTfzVsuH', 'Pada perlombaan tarik tambang, kelompok A menarik ke arah timur dengan gaya 700 N. Kelompok B menarik ke barat dengan gaya 665 N. Kelompok yang memenangi perlombaan adalah kelompok . . . .', 'A. A dengan resultan gaya 25 N', 'B. A dengan resultan gaya 35 N', 'C. B dengan resultan gaya 25 N', 'D. B dengan resultan gaya 35 N', 'B dengan resultan gaya 40 N'),
(13, 'BaMTfzVsuH', 'Dua buah gaya masing-masing 10 N dan15 N membentuk sudut 60°. Besar resultan kedua gaya tersebut adalah . .', 'A. 5 ?3 N ', 'B. 5 ?17 N ', 'C. 5 ?19 N', 'D. 5 ?2 N', '5 ?4 N'),
(14, 'qrkGPQdyxg', 'Gerakan suatu benda dimana setiap titik pada benda tersebut mempunyai jarak yang tetap terhadap suatu sumbu tertentu, merupakan pengertian dari:', 'A. Gerak Translasi', 'B. Gerak Rotasi', 'C. Perpindahan', 'D. Kelajuan', 'nomaden'),
(15, 'qrkGPQdyxg', 'Sebuah balok bermassa 1,5 kg didorong ke atas oleh gaya konstan F = 15 N pada bidang miring seperti gambar. Anggap percepatan gravitasi (g) 10 ms-2 dan gesekan antara balok dan bidang miring nol. Usaha total yang dilakukan pada balok adalah ...', 'A. 15 J', 'B. 30 J', 'C. 35 J', 'D. 45 J', '100 J'),
(16, 'qrkGPQdyxg', 'Sebanyak 3 liter gas Argon bersuhu 270C pada tekanan 1 atm (1 atm = 105 Pa) berada di dalam tabung. Jika konstanta gas umum R = 8,314 J m-1K-1 dan banyaknya partikel dalam 1 mol gas 6,02 x 1023 partikel, maka banyak partikel gas Argon dalam tabung tersebut adalah ...', 'A. 0,83 x 1023 partikel', 'B. 0,72 x 1023 partikel', 'C. 0,42 x 1023 partikel', 'D. 0,22 x 1023 partikel', '0.1 x 1000 partikel'),
(17, 'qrkGPQdyxg', 'Sebuah partikel berputar  dengan 240 rpm. Jika jari-jari lintasannya 2 m, maka kelajuan linier partikel tersebut adalah:', 'A. 4 ? m/s', 'B. 6 ? m/s', 'C. 8 ? m/s', 'D. 16 ? m/s', '18 ? m/s'),
(18, 'qrkGPQdyxg', 'Sebuah mobil mulai bergerak dari keadaan diam dengan percepatan tetap 24 m/s2. Maka kecepatan mobil setelah bergerak selama 18 sekon adalah:', 'A. 2 m/s', 'B. 24 m/s', 'C. 36 m/s', 'D. 42 m/s', '50 m/s'),
(19, 'qrkGPQdyxg', 'Suatu kereta api sedang bergerak dengan kelajuan 17 m/s. Ketika melewati tanda akan memasuki stasiun, masinis memperlambat kelajuan kereta api sebesar 2,00 m/s2. Waktu yang diperlukan kereta api sampai kereta api benar-benar berhenti adalah …', 'A. 3,5', 'B. 8', 'C. 8,5', 'D. 14', '90'),
(20, 'qrkGPQdyxg', 'Waktu yang diperlukan sebuah mobil yang bergerak dengan percepatan 2 m/s2, untuk mengubah kecepatannya dari 10 m/s menjadi 30 m/s adalah:', 'A. 10 s', 'B. 20 s', 'C. 30 s', 'D. 40 s', '11 s'),
(21, 'qrkGPQdyxg', 'Sebuah peluru dengan massa 20 gram ditembakkan pada sudut elevasi 600 dan kecepatan 40 m.s-   1 seperti gambar. Jika gesekan dengan udara diabaikan, maka energi kinetik peluru pada titik tertinggi adalah ...  ', 'A. 0 joule', 'B. 4 joule', 'C. 8 joule', 'D. 12 joule', '2 joule'),
(22, 'qrkGPQdyxg', 'Kelompok besaran di bawah ini yang termasuk besaran vektor adalah . . . .', 'A. kelajuan, kuat arus, gaya', 'B. energi, usaha, banyak mol zat', 'C. kecepatan, momentum, kuat arus listrik', 'D. tegangan, intensitas cahaya, gaya', 'tegangan listrik'),
(23, 'qrkGPQdyxg', 'Pada perlombaan tarik tambang, kelompok A menarik ke arah timur dengan gaya 700 N. Kelompok B menarik ke barat dengan gaya 665 N. Kelompok yang memenangi perlombaan adalah kelompok . . . .', 'A. A dengan resultan gaya 25 N', 'B. A dengan resultan gaya 35 N', 'C. B dengan resultan gaya 25 N', 'D. B dengan resultan gaya 35 N', 'B dengan resultan gaya 40 N'),
(24, 'qrkGPQdyxg', 'Dua buah gaya masing-masing 10 N dan15 N membentuk sudut 60°. Besar resultan kedua gaya tersebut adalah . .', 'A. 5 ?3 N ', 'B. 5 ?17 N ', 'C. 5 ?19 N', 'D. 5 ?2 N', '5 ?4 N'),
(25, 'eVIhNFH8Yv', 'Gerakan suatu benda dimana setiap titik pada benda tersebut mempunyai jarak yang tetap terhadap suatu sumbu tertentu, merupakan pengertian dari:', 'A. Gerak Translasi', 'B. Gerak Rotasi', 'C. Perpindahan', 'D. Kelajuan', 'nomaden'),
(26, 'eVIhNFH8Yv', 'Sebuah balok bermassa 1,5 kg didorong ke atas oleh gaya konstan F = 15 N pada bidang miring seperti gambar. Anggap percepatan gravitasi (g) 10 ms-2 dan gesekan antara balok dan bidang miring nol. Usaha total yang dilakukan pada balok adalah ...', 'A. 15 J', 'B. 30 J', 'C. 35 J', 'D. 45 J', '100 J'),
(27, 'eVIhNFH8Yv', 'Sebanyak 3 liter gas Argon bersuhu 270C pada tekanan 1 atm (1 atm = 105 Pa) berada di dalam tabung. Jika konstanta gas umum R = 8,314 J m-1K-1 dan banyaknya partikel dalam 1 mol gas 6,02 x 1023 partikel, maka banyak partikel gas Argon dalam tabung tersebut adalah ...', 'A. 0,83 x 1023 partikel', 'B. 0,72 x 1023 partikel', 'C. 0,42 x 1023 partikel', 'D. 0,22 x 1023 partikel', '0.1 x 1000 partikel'),
(28, 'eVIhNFH8Yv', 'Sebuah partikel berputar  dengan 240 rpm. Jika jari-jari lintasannya 2 m, maka kelajuan linier partikel tersebut adalah:', 'A. 4 ? m/s', 'B. 6 ? m/s', 'C. 8 ? m/s', 'D. 16 ? m/s', '18 ? m/s'),
(29, 'eVIhNFH8Yv', 'Sebuah mobil mulai bergerak dari keadaan diam dengan percepatan tetap 24 m/s2. Maka kecepatan mobil setelah bergerak selama 18 sekon adalah:', 'A. 2 m/s', 'B. 24 m/s', 'C. 36 m/s', 'D. 42 m/s', '50 m/s'),
(30, 'eVIhNFH8Yv', 'Suatu kereta api sedang bergerak dengan kelajuan 17 m/s. Ketika melewati tanda akan memasuki stasiun, masinis memperlambat kelajuan kereta api sebesar 2,00 m/s2. Waktu yang diperlukan kereta api sampai kereta api benar-benar berhenti adalah …', 'A. 3,5', 'B. 8', 'C. 8,5', 'D. 14', '90'),
(31, 'eVIhNFH8Yv', 'Waktu yang diperlukan sebuah mobil yang bergerak dengan percepatan 2 m/s2, untuk mengubah kecepatannya dari 10 m/s menjadi 30 m/s adalah:', 'A. 10 s', 'B. 20 s', 'C. 30 s', 'D. 40 s', '11 s'),
(32, 'eVIhNFH8Yv', 'Sebuah peluru dengan massa 20 gram ditembakkan pada sudut elevasi 600 dan kecepatan 40 m.s-   1 seperti gambar. Jika gesekan dengan udara diabaikan, maka energi kinetik peluru pada titik tertinggi adalah ...  ', 'A. 0 joule', 'B. 4 joule', 'C. 8 joule', 'D. 12 joule', '2 joule'),
(33, 'eVIhNFH8Yv', 'Kelompok besaran di bawah ini yang termasuk besaran vektor adalah . . . .', 'A. kelajuan, kuat arus, gaya', 'B. energi, usaha, banyak mol zat', 'C. kecepatan, momentum, kuat arus listrik', 'D. tegangan, intensitas cahaya, gaya', 'tegangan listrik'),
(34, 'eVIhNFH8Yv', 'Pada perlombaan tarik tambang, kelompok A menarik ke arah timur dengan gaya 700 N. Kelompok B menarik ke barat dengan gaya 665 N. Kelompok yang memenangi perlombaan adalah kelompok . . . .', 'A. A dengan resultan gaya 25 N', 'B. A dengan resultan gaya 35 N', 'C. B dengan resultan gaya 25 N', 'D. B dengan resultan gaya 35 N', 'B dengan resultan gaya 40 N'),
(35, 'eVIhNFH8Yv', 'Dua buah gaya masing-masing 10 N dan15 N membentuk sudut 60°. Besar resultan kedua gaya tersebut adalah . .', 'A. 5 ?3 N ', 'B. 5 ?17 N ', 'C. 5 ?19 N', 'D. 5 ?2 N', '5 ?4 N'),
(36, 'gP8RMp1Sk7', 'asdasd', 'A. A', 'B. A', 'C. A', 'D. A', 'A'),
(37, 'PnSoajKEbY', 'Gerakan suatu benda dimana setiap titik pada benda tersebut mempunyai jarak yang tetap terhadap suatu sumbu tertentu, merupakan pengertian dari:', 'A. Gerak Translasi', 'B. Gerak Rotasi', 'C. Perpindahan', 'D. Kelajuan', 'nomaden'),
(38, 'PnSoajKEbY', 'Sebuah balok bermassa 1,5 kg didorong ke atas oleh gaya konstan F = 15 N pada bidang miring seperti gambar. Anggap percepatan gravitasi (g) 10 ms-2 dan gesekan antara balok dan bidang miring nol. Usaha total yang dilakukan pada balok adalah ...', 'A. 15 J', 'B. 30 J', 'C. 35 J', 'D. 45 J', '100 J'),
(39, 'PnSoajKEbY', 'Sebanyak 3 liter gas Argon bersuhu 270C pada tekanan 1 atm (1 atm = 105 Pa) berada di dalam tabung. Jika konstanta gas umum R = 8,314 J m-1K-1 dan banyaknya partikel dalam 1 mol gas 6,02 x 1023 partikel, maka banyak partikel gas Argon dalam tabung tersebut adalah ...', 'A. 0,83 x 1023 partikel', 'B. 0,72 x 1023 partikel', 'C. 0,42 x 1023 partikel', 'D. 0,22 x 1023 partikel', '0.1 x 1000 partikel'),
(40, 'PnSoajKEbY', 'Sebuah partikel berputar  dengan 240 rpm. Jika jari-jari lintasannya 2 m, maka kelajuan linier partikel tersebut adalah:', 'A. 4 ? m/s', 'B. 6 ? m/s', 'C. 8 ? m/s', 'D. 16 ? m/s', '18 ? m/s'),
(41, 'PnSoajKEbY', 'Sebuah mobil mulai bergerak dari keadaan diam dengan percepatan tetap 24 m/s2. Maka kecepatan mobil setelah bergerak selama 18 sekon adalah:', 'A. 2 m/s', 'B. 24 m/s', 'C. 36 m/s', 'D. 42 m/s', '50 m/s'),
(42, 'PnSoajKEbY', 'Suatu kereta api sedang bergerak dengan kelajuan 17 m/s. Ketika melewati tanda akan memasuki stasiun, masinis memperlambat kelajuan kereta api sebesar 2,00 m/s2. Waktu yang diperlukan kereta api sampai kereta api benar-benar berhenti adalah …', 'A. 3,5', 'B. 8', 'C. 8,5', 'D. 14', '90'),
(43, 'PnSoajKEbY', 'Waktu yang diperlukan sebuah mobil yang bergerak dengan percepatan 2 m/s2, untuk mengubah kecepatannya dari 10 m/s menjadi 30 m/s adalah:', 'A. 10 s', 'B. 20 s', 'C. 30 s', 'D. 40 s', '11 s'),
(44, 'PnSoajKEbY', 'Sebuah peluru dengan massa 20 gram ditembakkan pada sudut elevasi 600 dan kecepatan 40 m.s-   1 seperti gambar. Jika gesekan dengan udara diabaikan, maka energi kinetik peluru pada titik tertinggi adalah ...  ', 'A. 0 joule', 'B. 4 joule', 'C. 8 joule', 'D. 12 joule', '2 joule'),
(45, 'PnSoajKEbY', 'Kelompok besaran di bawah ini yang termasuk besaran vektor adalah . . . .', 'A. kelajuan, kuat arus, gaya', 'B. energi, usaha, banyak mol zat', 'C. kecepatan, momentum, kuat arus listrik', 'D. tegangan, intensitas cahaya, gaya', 'tegangan listrik'),
(46, 'PnSoajKEbY', 'Pada perlombaan tarik tambang, kelompok A menarik ke arah timur dengan gaya 700 N. Kelompok B menarik ke barat dengan gaya 665 N. Kelompok yang memenangi perlombaan adalah kelompok . . . .', 'A. A dengan resultan gaya 25 N', 'B. A dengan resultan gaya 35 N', 'C. B dengan resultan gaya 25 N', 'D. B dengan resultan gaya 35 N', 'B dengan resultan gaya 40 N'),
(47, 'PnSoajKEbY', 'Dua buah gaya masing-masing 10 N dan15 N membentuk sudut 60°. Besar resultan kedua gaya tersebut adalah . .', 'A. 5 ?3 N ', 'B. 5 ?17 N ', 'C. 5 ?19 N', 'D. 5 ?2 N', '5 ?4 N'),
(48, 'eqnzJEF6Vk', 'Gerakan suatu benda dimana setiap titik pada benda tersebut mempunyai jarak yang tetap terhadap suatu sumbu tertentu, merupakan pengertian dari:', 'A. Gerak Translasi', 'B. Gerak Rotasi', 'C. Perpindahan', 'D. Kelajuan', 'nomaden'),
(49, 'eqnzJEF6Vk', 'Sebuah balok bermassa 1,5 kg didorong ke atas oleh gaya konstan F = 15 N pada bidang miring seperti gambar. Anggap percepatan gravitasi (g) 10 ms-2 dan gesekan antara balok dan bidang miring nol. Usaha total yang dilakukan pada balok adalah ...', 'A. 15 J', 'B. 30 J', 'C. 35 J', 'D. 45 J', '100 J'),
(50, 'eqnzJEF6Vk', 'Sebanyak 3 liter gas Argon bersuhu 270C pada tekanan 1 atm (1 atm = 105 Pa) berada di dalam tabung. Jika konstanta gas umum R = 8,314 J m-1K-1 dan banyaknya partikel dalam 1 mol gas 6,02 x 1023 partikel, maka banyak partikel gas Argon dalam tabung tersebut adalah ...', 'A. 0,83 x 1023 partikel', 'B. 0,72 x 1023 partikel', 'C. 0,42 x 1023 partikel', 'D. 0,22 x 1023 partikel', '0.1 x 1000 partikel'),
(51, 'eqnzJEF6Vk', 'Sebuah partikel berputar  dengan 240 rpm. Jika jari-jari lintasannya 2 m, maka kelajuan linier partikel tersebut adalah:', 'A. 4 ? m/s', 'B. 6 ? m/s', 'C. 8 ? m/s', 'D. 16 ? m/s', '18 ? m/s'),
(52, 'eqnzJEF6Vk', 'Sebuah mobil mulai bergerak dari keadaan diam dengan percepatan tetap 24 m/s2. Maka kecepatan mobil setelah bergerak selama 18 sekon adalah:', 'A. 2 m/s', 'B. 24 m/s', 'C. 36 m/s', 'D. 42 m/s', '50 m/s'),
(53, 'eqnzJEF6Vk', 'Suatu kereta api sedang bergerak dengan kelajuan 17 m/s. Ketika melewati tanda akan memasuki stasiun, masinis memperlambat kelajuan kereta api sebesar 2,00 m/s2. Waktu yang diperlukan kereta api sampai kereta api benar-benar berhenti adalah …', 'A. 3,5', 'B. 8', 'C. 8,5', 'D. 14', '90'),
(54, 'eqnzJEF6Vk', 'Waktu yang diperlukan sebuah mobil yang bergerak dengan percepatan 2 m/s2, untuk mengubah kecepatannya dari 10 m/s menjadi 30 m/s adalah:', 'A. 10 s', 'B. 20 s', 'C. 30 s', 'D. 40 s', '11 s'),
(55, 'eqnzJEF6Vk', 'Sebuah peluru dengan massa 20 gram ditembakkan pada sudut elevasi 600 dan kecepatan 40 m.s-   1 seperti gambar. Jika gesekan dengan udara diabaikan, maka energi kinetik peluru pada titik tertinggi adalah ...  ', 'A. 0 joule', 'B. 4 joule', 'C. 8 joule', 'D. 12 joule', '2 joule'),
(56, 'eqnzJEF6Vk', 'Kelompok besaran di bawah ini yang termasuk besaran vektor adalah . . . .', 'A. kelajuan, kuat arus, gaya', 'B. energi, usaha, banyak mol zat', 'C. kecepatan, momentum, kuat arus listrik', 'D. tegangan, intensitas cahaya, gaya', 'tegangan listrik'),
(57, 'eqnzJEF6Vk', 'Pada perlombaan tarik tambang, kelompok A menarik ke arah timur dengan gaya 700 N. Kelompok B menarik ke barat dengan gaya 665 N. Kelompok yang memenangi perlombaan adalah kelompok . . . .', 'A. A dengan resultan gaya 25 N', 'B. A dengan resultan gaya 35 N', 'C. B dengan resultan gaya 25 N', 'D. B dengan resultan gaya 35 N', 'B dengan resultan gaya 40 N'),
(58, 'eqnzJEF6Vk', 'Dua buah gaya masing-masing 10 N dan15 N membentuk sudut 60°. Besar resultan kedua gaya tersebut adalah . .', 'A. 5 ?3 N ', 'B. 5 ?17 N ', 'C. 5 ?19 N', 'D. 5 ?2 N', '5 ?4 N'),
(59, 'EADCkpsIqa', 'Siapa presiden indonesia sekarang', 'A. Jokowi ', 'B. Habibi', 'C. Dika', 'D. Madani', 'A'),
(60, 'ZjG1iqK49F', 'asdasdasdsa', 'A. a', 'B. a', 'C. a', 'D. a', 'A'),
(61, 'K5AiPWQrj6', 'Siapa Presiden Indonesia sekarang', 'A. Jokowi', 'B. Megawati', 'C. Soekarno', 'D. Soeharto', 'A'),
(62, '3NsWDbmGei', 'Siapa Presiden Indonesia', 'A. Jokowi', 'B. B', 'C. C', 'D. D', 'A'),
(63, '3NsWDbmGei', 'Siapa Penemu Bomb', 'A. A', 'B. B', 'C. C', 'D. D', 'B'),
(64, 'uB9fdp8wvi', '1 + 1', 'A. 2', 'B. 3', 'C. 4', 'D. 5', 'A'),
(65, 'uB9fdp8wvi', '2+2', 'A. 2', 'B. 4', 'C. 6', 'D. 3', 'B');

-- --------------------------------------------------------

--
-- Table structure for table `ujian_siswa`
--

CREATE TABLE `ujian_siswa` (
  `id_ujian_siswa` int(11) NOT NULL,
  `ujian_id` int(11) NOT NULL,
  `ujian` varchar(100) NOT NULL,
  `siswa` int(11) NOT NULL,
  `jawaban` varchar(100) DEFAULT NULL,
  `benar` int(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ujian_siswa`
--

INSERT INTO `ujian_siswa` (`id_ujian_siswa`, `ujian_id`, `ujian`, `siswa`, `jawaban`, `benar`) VALUES
(1, 1, 'JwhLunEjeY', 18, 'A', 1),
(2, 2, 'JwhLunEjeY', 18, 'B', 1),
(3, 3, 'BaMTfzVsuH', 18, NULL, 2),
(4, 4, 'BaMTfzVsuH', 18, NULL, 2),
(5, 5, 'BaMTfzVsuH', 18, NULL, 2),
(6, 6, 'BaMTfzVsuH', 18, NULL, 2),
(7, 7, 'BaMTfzVsuH', 18, NULL, 2),
(8, 8, 'BaMTfzVsuH', 18, NULL, 2),
(9, 9, 'BaMTfzVsuH', 18, NULL, 2),
(10, 10, 'BaMTfzVsuH', 18, NULL, 2),
(11, 11, 'BaMTfzVsuH', 18, NULL, 2),
(12, 12, 'BaMTfzVsuH', 18, NULL, 2),
(13, 13, 'BaMTfzVsuH', 18, NULL, 2),
(14, 14, 'qrkGPQdyxg', 18, NULL, 2),
(15, 15, 'qrkGPQdyxg', 18, NULL, 2),
(16, 16, 'qrkGPQdyxg', 18, NULL, 2),
(17, 17, 'qrkGPQdyxg', 18, NULL, 2),
(18, 18, 'qrkGPQdyxg', 18, NULL, 2),
(19, 19, 'qrkGPQdyxg', 18, NULL, 2),
(20, 20, 'qrkGPQdyxg', 18, NULL, 2),
(21, 21, 'qrkGPQdyxg', 18, NULL, 2),
(22, 22, 'qrkGPQdyxg', 18, NULL, 2),
(23, 23, 'qrkGPQdyxg', 18, NULL, 2),
(24, 24, 'qrkGPQdyxg', 18, NULL, 2),
(25, 25, 'eVIhNFH8Yv', 18, NULL, NULL),
(26, 26, 'eVIhNFH8Yv', 18, NULL, NULL),
(27, 27, 'eVIhNFH8Yv', 18, NULL, NULL),
(28, 28, 'eVIhNFH8Yv', 18, NULL, NULL),
(29, 29, 'eVIhNFH8Yv', 18, NULL, NULL),
(30, 30, 'eVIhNFH8Yv', 18, NULL, NULL),
(31, 31, 'eVIhNFH8Yv', 18, NULL, NULL),
(32, 32, 'eVIhNFH8Yv', 18, NULL, NULL),
(33, 33, 'eVIhNFH8Yv', 18, NULL, NULL),
(34, 34, 'eVIhNFH8Yv', 18, NULL, NULL),
(35, 35, 'eVIhNFH8Yv', 18, NULL, NULL),
(36, 36, 'gP8RMp1Sk7', 18, NULL, NULL),
(37, 37, 'PnSoajKEbY', 18, 'A', 0),
(38, 38, 'PnSoajKEbY', 18, 'B', 0),
(39, 39, 'PnSoajKEbY', 18, 'A', 0),
(40, 40, 'PnSoajKEbY', 18, NULL, 2),
(41, 41, 'PnSoajKEbY', 18, NULL, 2),
(42, 42, 'PnSoajKEbY', 18, NULL, 2),
(43, 43, 'PnSoajKEbY', 18, NULL, 2),
(44, 44, 'PnSoajKEbY', 18, NULL, 2),
(45, 45, 'PnSoajKEbY', 18, NULL, 2),
(46, 46, 'PnSoajKEbY', 18, NULL, 2),
(47, 47, 'PnSoajKEbY', 18, NULL, 2),
(48, 48, 'eqnzJEF6Vk', 18, 'A', 0),
(49, 49, 'eqnzJEF6Vk', 18, 'A', 0),
(50, 50, 'eqnzJEF6Vk', 18, 'B', 0),
(51, 51, 'eqnzJEF6Vk', 18, 'B', 0),
(52, 52, 'eqnzJEF6Vk', 18, 'B', 0),
(53, 53, 'eqnzJEF6Vk', 18, 'B', 0),
(54, 54, 'eqnzJEF6Vk', 18, 'D', 0),
(55, 55, 'eqnzJEF6Vk', 18, 'C', 0),
(56, 56, 'eqnzJEF6Vk', 18, 'D', 0),
(57, 57, 'eqnzJEF6Vk', 18, 'C', 0),
(58, 58, 'eqnzJEF6Vk', 18, 'C', 0),
(59, 59, 'EADCkpsIqa', 20, 'A', 1),
(60, 60, 'ZjG1iqK49F', 20, NULL, NULL),
(61, 61, 'K5AiPWQrj6', 22, 'A', 1),
(62, 62, '3NsWDbmGei', 22, 'A', 1),
(63, 63, '3NsWDbmGei', 22, 'D', 0),
(64, 64, 'uB9fdp8wvi', 3, 'A', 1),
(65, 64, 'uB9fdp8wvi', 4, NULL, 2),
(66, 64, 'uB9fdp8wvi', 5, NULL, NULL),
(67, 64, 'uB9fdp8wvi', 6, NULL, NULL),
(68, 65, 'uB9fdp8wvi', 3, 'D', 0),
(69, 65, 'uB9fdp8wvi', 4, NULL, 2),
(70, 65, 'uB9fdp8wvi', 5, NULL, NULL),
(71, 65, 'uB9fdp8wvi', 6, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id_admin`);

--
-- Indexes for table `chat_materi`
--
ALTER TABLE `chat_materi`
  ADD PRIMARY KEY (`id_chat_materi`);

--
-- Indexes for table `chat_tugas`
--
ALTER TABLE `chat_tugas`
  ADD PRIMARY KEY (`id_chat_tugas`);

--
-- Indexes for table `file`
--
ALTER TABLE `file`
  ADD PRIMARY KEY (`id_file`);

--
-- Indexes for table `guru`
--
ALTER TABLE `guru`
  ADD PRIMARY KEY (`id_guru`);

--
-- Indexes for table `guru_kelas`
--
ALTER TABLE `guru_kelas`
  ADD PRIMARY KEY (`id_guru_kelas`);

--
-- Indexes for table `guru_mapel`
--
ALTER TABLE `guru_mapel`
  ADD PRIMARY KEY (`id_guru_mapel`);

--
-- Indexes for table `kelas`
--
ALTER TABLE `kelas`
  ADD PRIMARY KEY (`id_kelas`);

--
-- Indexes for table `mapel`
--
ALTER TABLE `mapel`
  ADD PRIMARY KEY (`id_mapel`);

--
-- Indexes for table `materi`
--
ALTER TABLE `materi`
  ADD PRIMARY KEY (`id_materi`);

--
-- Indexes for table `materi_siswa`
--
ALTER TABLE `materi_siswa`
  ADD PRIMARY KEY (`id_materi_siswa`);

--
-- Indexes for table `ortu`
--
ALTER TABLE `ortu`
  ADD PRIMARY KEY (`id_ortu`);

--
-- Indexes for table `rekap`
--
ALTER TABLE `rekap`
  ADD PRIMARY KEY (`id_rekap`);

--
-- Indexes for table `siswa`
--
ALTER TABLE `siswa`
  ADD PRIMARY KEY (`id_siswa`);

--
-- Indexes for table `tugas`
--
ALTER TABLE `tugas`
  ADD PRIMARY KEY (`id_tugas`);

--
-- Indexes for table `tugas_siswa`
--
ALTER TABLE `tugas_siswa`
  ADD PRIMARY KEY (`id_tugas_siswa`);

--
-- Indexes for table `ujian`
--
ALTER TABLE `ujian`
  ADD PRIMARY KEY (`id_ujian`);

--
-- Indexes for table `ujian_detail`
--
ALTER TABLE `ujian_detail`
  ADD PRIMARY KEY (`id_detail_ujian`);

--
-- Indexes for table `ujian_siswa`
--
ALTER TABLE `ujian_siswa`
  ADD PRIMARY KEY (`id_ujian_siswa`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id_admin` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `chat_materi`
--
ALTER TABLE `chat_materi`
  MODIFY `id_chat_materi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=74;

--
-- AUTO_INCREMENT for table `chat_tugas`
--
ALTER TABLE `chat_tugas`
  MODIFY `id_chat_tugas` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `file`
--
ALTER TABLE `file`
  MODIFY `id_file` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;

--
-- AUTO_INCREMENT for table `guru`
--
ALTER TABLE `guru`
  MODIFY `id_guru` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `guru_kelas`
--
ALTER TABLE `guru_kelas`
  MODIFY `id_guru_kelas` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `guru_mapel`
--
ALTER TABLE `guru_mapel`
  MODIFY `id_guru_mapel` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `kelas`
--
ALTER TABLE `kelas`
  MODIFY `id_kelas` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `mapel`
--
ALTER TABLE `mapel`
  MODIFY `id_mapel` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `materi`
--
ALTER TABLE `materi`
  MODIFY `id_materi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `materi_siswa`
--
ALTER TABLE `materi_siswa`
  MODIFY `id_materi_siswa` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `ortu`
--
ALTER TABLE `ortu`
  MODIFY `id_ortu` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `rekap`
--
ALTER TABLE `rekap`
  MODIFY `id_rekap` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `siswa`
--
ALTER TABLE `siswa`
  MODIFY `id_siswa` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `tugas`
--
ALTER TABLE `tugas`
  MODIFY `id_tugas` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tugas_siswa`
--
ALTER TABLE `tugas_siswa`
  MODIFY `id_tugas_siswa` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `ujian`
--
ALTER TABLE `ujian`
  MODIFY `id_ujian` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `ujian_detail`
--
ALTER TABLE `ujian_detail`
  MODIFY `id_detail_ujian` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=66;

--
-- AUTO_INCREMENT for table `ujian_siswa`
--
ALTER TABLE `ujian_siswa`
  MODIFY `id_ujian_siswa` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=72;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
